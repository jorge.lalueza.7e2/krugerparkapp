package cat.itb.krugerparkapp.adapter;


import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import cat.itb.krugerparkapp.R;
import cat.itb.krugerparkapp.model.KruegerUser;
import cat.itb.krugerparkapp.model.Vista;
import cat.itb.krugerparkapp.repository.FirestoreRepository;
import cat.itb.krugerparkapp.repository.SnapshotCallback;
import cat.itb.krugerparkapp.ui.main.TimelineActivity;


public class VistaAdapater extends FirestoreAdapter<VistaAdapater.ViewHolder> {

    private FirestoreRepository firestoreRepository = new FirestoreRepository( FirebaseFirestore.getInstance() );
    private FirebaseAuth mAtuh = FirebaseAuth.getInstance();



    public interface OnVistaSelectedListener {
        void onVistaClickedListener(DocumentSnapshot vista);
    }

    private OnVistaSelectedListener mListener;

    public VistaAdapater(Query query, OnVistaSelectedListener listener) {
        super( query );
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );
        return new ViewHolder( inflater.inflate( R.layout.publi_row, parent, false ) );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DocumentSnapshot vistaSnapshot = getSnapshot(position);
        holder.bind( vistaSnapshot, mListener );
        holder.imageOptions.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialogPersonalizado = new Dialog(v.getContext());
                dialogPersonalizado.setContentView(R.layout.dialog_options);
                Button btnEliminar = dialogPersonalizado.findViewById(R.id.eliminarBtn);
                if (mAtuh.getUid().equals(vistaSnapshot.get("userID"))){
                    btnEliminar.setVisibility(View.VISIBLE);
                    btnEliminar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            firestoreRepository.deleteDocument("vistas", vistaSnapshot.getId());
                            Toast.makeText(v.getContext(), "Publicación eliminada", Toast.LENGTH_SHORT).show();
                            dialogPersonalizado.dismiss();
                            //Llamar metodo eliminar
                        }
                    });
                }


                Button btnCompartir = dialogPersonalizado.findViewById(R.id.btnCompartir);
                btnCompartir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(v.getContext(), "Publicación compartida", Toast.LENGTH_SHORT).show();
                        final Dialog dialogCompartir = new Dialog(v.getContext());
                        dialogCompartir.setContentView(R.layout.dialog_compartir );
                        ImageView imageGmail = dialogCompartir.findViewById( R.id.gmailBtn );
                        imageGmail.setOnClickListener( new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        } );
                        ImageView imageInstagram = dialogCompartir.findViewById( R.id.instagramBtn );
                        imageInstagram.setOnClickListener( new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        } );
                        ImageView imageFacebook = dialogCompartir.findViewById( R.id.facebookBtn );
                        imageFacebook.setOnClickListener( new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        } );

                        dialogCompartir.show();

                    }
                });

                Button btnDenunciar = dialogPersonalizado.findViewById(R.id.btnDenunciar);
                btnDenunciar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(v.getContext(), "Publicación denunciada", Toast.LENGTH_SHORT).show();
                    }
                });

                Button btnCancelar = dialogPersonalizado.findViewById( R.id.btnCancelar );
                btnCancelar.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogPersonalizado.cancel();
                    }
                } );
                dialogPersonalizado.show();
            }
        } );
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        FirestoreRepository firestoreRepository = new FirestoreRepository( FirebaseFirestore.getInstance() );

        ImageView userPicture;
        TextView userName;
        ImageView postPicture;
        TextView dateView;
        TextView description;

        ImageView imageOptions;

        public ViewHolder(@NonNull View itemView) {
            super( itemView );

            userPicture = itemView.findViewById( R.id.userPicture );
            userName = itemView.findViewById( R.id.username );
            postPicture = itemView.findViewById( R.id.postPicture );
            dateView = itemView.findViewById( R.id.creationDate );
            description = itemView.findViewById( R.id.postDescription );
            imageOptions = itemView.findViewById( R.id.imageOptions );
        }

        public void bind(final DocumentSnapshot snapshot, final OnVistaSelectedListener listener) {

            Vista vista = snapshot.toObject(Vista.class);
            firestoreRepository.getDocumentByField("users", "id", vista.getUserID(), new SnapshotCallback() {
                @Override
                public void onSnapshot(DocumentSnapshot documentSnapshot) {
                    KruegerUser firebaseUser = documentSnapshot.toObject( KruegerUser.class );

                    Glide.with(userPicture.getContext())
                            .load(firebaseUser.getPhotoUrl())
                            .into(userPicture);
                    userName.setText(firebaseUser.getDisplayName());
                }

                @Override
                public void onSnapshots(List<DocumentSnapshot> documentSnapshotList) {

                }
            });

            Glide.with(postPicture.getContext())
                    .load(vista.getPhoto())
                    .into(postPicture);


            Date date = vista.getDate().toDate();
            String dateString = new SimpleDateFormat( "dd/MM/yyyy" ).format( date );
            dateView.setText( dateString );
            description.setText( vista.getDescription() );
            // Click listener
            itemView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onVistaClickedListener( snapshot );
                    }
                }
            } );

            }
        }
    }


