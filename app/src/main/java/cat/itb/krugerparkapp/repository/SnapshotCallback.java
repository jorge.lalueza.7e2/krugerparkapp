package cat.itb.krugerparkapp.repository;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.List;

public interface SnapshotCallback {
    void onSnapshot(DocumentSnapshot documentSnapshot);

    void onSnapshots(List<DocumentSnapshot> documentSnapshotList);
}
