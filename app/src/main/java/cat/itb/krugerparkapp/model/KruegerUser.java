package cat.itb.krugerparkapp.model;

public class KruegerUser {

    private String userID;
    private String displayName;
    private String email;
    private String photoUrl;

    public KruegerUser(String userID, String displayName, String email, String photoUrl) {
        this.userID = userID;
        this.displayName = displayName;
        this.email = email;
        this.photoUrl = photoUrl;
    }

    public KruegerUser(){

    }

    public String getUserID() {
        return userID;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }
}
