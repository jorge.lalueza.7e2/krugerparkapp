package cat.itb.krugerparkapp.model;

import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.HashMap;

public class Vista implements Serializable {

    final FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    private DocumentReference animal;
    private GeoPoint location;
    private String photo;
    private String userID;
    private Timestamp date;
    private String description;

    public Vista(String animal, GeoPoint location, Uri photo, String userID, Timestamp date, String description) {
        this.animal = firestore.collection("listaAnimales").document(animal);
        this.location = location;
        this.photo = photo.toString();
        this.userID = userID;
        this.date = date;
        this.description = description;
    }

    public Vista(){

    }

    public DocumentReference  getAnimal() {
        return animal;
    }

    public void setAnimal(DocumentReference animal) {
        this.animal = animal;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Timestamp getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }


}
