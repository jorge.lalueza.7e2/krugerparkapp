package cat.itb.krugerparkapp.model;

import android.widget.ImageView;

import java.io.Serializable;

public class Animal implements Serializable {
    private int numberId;
    private String name;
    private int imageAnimal;
    private int numberAnimal;
    private int color;

    public Animal(int numberId, String name, int imageAnimal, int numberAnimal, int color) {
        this.numberId = numberId;
        this.name = name;
        this.imageAnimal = imageAnimal;
        this.numberAnimal = numberAnimal;
        this.color = color;
    }

    public int getImageAnimal() {
        return imageAnimal;
    }

    public void setImageAnimal(int imageAnimal) {
        this.imageAnimal = imageAnimal;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }


    public int getNumberId() {
        return numberId;
    }

    public void setNumberId(int numberId) {
        this.numberId = numberId;
    }

    public int getNumberAnimal() {
        return numberAnimal;
    }

    public void setNumberAnimal(int numberAnimal) {
        this.numberAnimal = numberAnimal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
