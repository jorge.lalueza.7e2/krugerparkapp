package cat.itb.krugerparkapp.ui.main;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import cat.itb.krugerparkapp.R;
import cat.itb.krugerparkapp.adapter.VistaAdapater;

public class TimelineActivity extends AppCompatActivity implements VistaAdapater.OnVistaSelectedListener{

    private static final String KEY_VIEW_ID = "vista_id";

    private int selectedPosition = -1;

    private RecyclerView vistaRecycler;


    private FirebaseFirestore firestore;
    private Query mQuery;

    private VistaAdapater mAdapter;

    private ImageView imageView;
    private MainViewModel mViewModel;

    public static TimelineActivity newInstance() {
        return new TimelineActivity();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeline_activity);


        vistaRecycler = findViewById(R.id.animalsRecylerView);


        mViewModel = new ViewModelProvider( this ).get( MainViewModel.class );

        firestore = FirebaseFirestore.getInstance();

        //Obtener lista de publicaciones
        mQuery = firestore.collection("vistas")
                .orderBy("date", Query.Direction.DESCENDING);
        // RecyclerView
        mAdapter = new VistaAdapater(mQuery, this) {
            @Override
            protected void onDataChanged() {
                // Show/hide content if the query returns empty.
                if (getItemCount() == 0) {
                    vistaRecycler.setVisibility(View.GONE);
                    Snackbar.make(findViewById(android.R.id.content),
                            "There's no posts at all... Be the first !", Snackbar.LENGTH_LONG).show();
                } else {
                    vistaRecycler.setVisibility(View.VISIBLE);
                    String selectedVistaID = getIntent().getStringExtra(KEY_VIEW_ID);
                    if (selectedVistaID != null){
                        for (int i = 0; i < getItemCount(); i++){
                            DocumentSnapshot currentVista = getSnapshot(i);
                            if (selectedVistaID.equals(currentVista.getId())){
                                selectedPosition = i;
                            }
                        }
                        if (selectedPosition > -1){
                            vistaRecycler.smoothScrollToPosition(selectedPosition);
                        }
                    }
                }
            }

            @Override
            protected void onError(FirebaseFirestoreException e) {
                // Show a snackbar on errors
                Snackbar.make(findViewById(android.R.id.content),
                        "Error: check logs for info.", Snackbar.LENGTH_LONG).show();
            }
        };
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        vistaRecycler.setLayoutManager(layoutManager);
        vistaRecycler.setAdapter(mAdapter);


    }




    public void onStart() {
        super.onStart();

        // Apply filters
        //onFilter(mViewModel.getFilters());

        // Start listening for Firestore updates
        if (mAdapter != null) {
            mAdapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }

    @Override
    public void onVistaClickedListener(DocumentSnapshot vista) {

    }
}
