package cat.itb.krugerparkapp.ui.main;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cat.itb.krugerparkapp.R;

import cat.itb.krugerparkapp.model.Vista;
import cat.itb.krugerparkapp.repository.FirestoreRepository;
import cat.itb.krugerparkapp.service.MyDownloadService;
import cat.itb.krugerparkapp.service.MyUploadService;

public class AddActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "AddActivity";
    Spinner spinnerLocation;
    Spinner spinnerAnimals;
    TextView textLocationOption;
    MapsActivity mapsActivity;
    Bitmap imageBitmap;
    ImageView previewPicture;

    private BroadcastReceiver mBroadcastReceiver;
    private ProgressBar mProgressBar;
    private TextView mCaption;

    LatLng optionLatLng;
    String choosedOptionString;

    Bundle bundle;



    FirebaseStorage storage;
    StorageReference storageRef;
    private FirebaseFirestore firestoreDB;
    private FirebaseAuth mAuth;
    private FirestoreRepository fsRepository;

    private Uri mDownloadUrl = null;
    private Uri mFileUri = null;

    private Vista vista;

    private String mCurrentPhotoPath;

    private int requestType;

    private final int PERMISSION_ALL = 2;
    String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA
    };

    private static final int RC_IMAGE_CAPTURE = 1;
    private static final int RC_PICK_PICTURE = 101;

    private static final String KEY_FILE_URI = "key_file_uri";
    private static final String KEY_DOWNLOAD_URL = "key_download_url";

    private static final String KEY_GEOLOCATION = "GeoLoc";
    private static final String KEY_CHOSEN_LOCATION = "CustomLoc";
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        mapsActivity = new MapsActivity();
        setContentView( R.layout.activity_add );

        MaterialButton btnCamera = findViewById( R.id.hacerFoto);
        btnCamera.setOnClickListener(this);

        MaterialButton btnInsert = findViewById( R.id.btn_InsertarVista);
        btnInsert.setOnClickListener(this);

        previewPicture = findViewById(R.id.photoAddbtn);

        mProgressBar = findViewById(R.id.progressBar);
        mCaption = findViewById(R.id.caption);

        spinnerLocation = (Spinner)findViewById(R.id.spinnerLocation);
        spinnerAnimals = (Spinner)findViewById(R.id.spinnerAnimals);

        textLocationOption = findViewById(R.id.textLocationOption);

        //Instances
        firestoreDB = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();

        //References
        storageRef = storage.getReference();
        StorageReference folderRef = storageRef.child("usersUploads");
        mAuth = FirebaseAuth.getInstance();

       fsRepository = new FirestoreRepository(firestoreDB);

        bundle = getIntent().getExtras();
        if (bundle.size()==1){
            spinnerLocation.setVisibility(View.GONE);
            optionLatLng = (bundle.getParcelable(KEY_GEOLOCATION)!=null) ? bundle.getParcelable(KEY_GEOLOCATION) : bundle.getParcelable(KEY_CHOSEN_LOCATION);
            choosedOptionString = (bundle.getParcelable(KEY_GEOLOCATION)!=null) ? getResources().getStringArray(R.array.arrayLocationOptions)[0] : getResources().getStringArray(R.array.arrayLocationOptions)[1];
            textLocationOption.setText(choosedOptionString);
        }else {
            textLocationOption.setVisibility(View.GONE);
        }

        if (savedInstanceState != null) {
            mFileUri = savedInstanceState.getParcelable(KEY_FILE_URI);
            mDownloadUrl = savedInstanceState.getParcelable(KEY_DOWNLOAD_URL);
        }
        onNewIntent(getIntent());

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "onReceive:" + intent);
                hideProgressBar();

                switch (intent.getAction()) {
                    case MyDownloadService.DOWNLOAD_COMPLETED:
                        // Get number of bytes downloaded
                        long numBytes = intent.getLongExtra(MyDownloadService.EXTRA_BYTES_DOWNLOADED, 0);

                        // Alert success
                        showMessageDialog(getString(R.string.success), String.format(Locale.getDefault(),
                                "%d bytes downloaded from %s",
                                numBytes,
                                intent.getStringExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH)));
                        break;
                    case MyDownloadService.DOWNLOAD_ERROR:
                        // Alert failure
                        showMessageDialog("Error", String.format(Locale.getDefault(),
                                "Failed to download from %s",
                                intent.getStringExtra(MyDownloadService.EXTRA_DOWNLOAD_PATH)));
                        break;
                    case MyUploadService.UPLOAD_COMPLETED:
                        onUploadResultIntent(intent);
                        String animal = spinnerAnimals.getSelectedItem().toString();
                        GeoPoint geoPoint = new GeoPoint(optionLatLng.latitude, optionLatLng.longitude);
                        vista = new Vista(animal, geoPoint, mDownloadUrl, mAuth.getUid(), new Timestamp(Calendar.getInstance().getTime()),"#" + animal);
                        Map<String,Object> viewDocument = getViewDocument(vista);
                        fsRepository.addDocument("vistas", viewDocument);
                        Intent successIntent = new Intent(AddActivity.this, TimelineActivity.class);
                        startActivity(successIntent);
                        finish();
                    case MyUploadService.UPLOAD_ERROR:
                        onUploadResultIntent(intent);
                        break;
                }
            }
        };



    }

    private void onUploadResultIntent(Intent intent) {
        // Got a new intent from MyUploadService with a success or failure
        mDownloadUrl = intent.getParcelableExtra(MyUploadService.EXTRA_DOWNLOAD_URL);
        mFileUri = intent.getParcelableExtra(MyUploadService.EXTRA_FILE_URI);

    }

    private void launchGallery() {
        Log.d(TAG, "Launch_Gallery");

        // Pick an image from storage
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/*");
        startActivityForResult(intent, RC_PICK_PICTURE);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        // Check if this Activity was launched by clicking on an upload notification
        if (intent.hasExtra(MyUploadService.EXTRA_DOWNLOAD_URL)) {
            onUploadResultIntent(intent);
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        // Register receiver for uploads and downloads
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.registerReceiver(mBroadcastReceiver, MyDownloadService.getIntentFilter());
        manager.registerReceiver(mBroadcastReceiver, MyUploadService.getIntentFilter());
    }

    @Override
    public void onStop() {
        super.onStop();

        // Unregister download receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle out) {
        super.onSaveInstanceState(out);
        out.putParcelable(KEY_FILE_URI, mFileUri);
        out.putParcelable(KEY_DOWNLOAD_URL, mDownloadUrl);
    }

    private void selectImage(Context context) {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                        Toast.makeText(AddActivity.this, R.string.mssingCameraPermission, Toast.LENGTH_LONG).show();
                        return;
                    }
                    dispatchTakePictureIntent();
                } else if (options[item].equals("Choose from Gallery")) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        Toast.makeText(AddActivity.this, R.string.missingStoragePermission, Toast.LENGTH_LONG).show();
                        return;
                    }
                    launchGallery();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        super.onActivityResult(requestCode,resultCode, data);
        switch (requestCode){
            case RC_PICK_PICTURE:
                if (resultCode == RESULT_OK) {
                    mFileUri = data.getData();
                    if (mFileUri != null) {
                        previewPicture.setImageURI(mFileUri);
                        requestType = RC_PICK_PICTURE;
                    } else {
                        Log.w(TAG, "File URI is null");
                    }
                } else {
                    Toast.makeText(this, "Taking picture failed.", Toast.LENGTH_SHORT).show();
                }
                break;

            case RC_IMAGE_CAPTURE:
                if (resultCode == RESULT_OK) {
                    File file = new File(mCurrentPhotoPath);
                    previewPicture.setImageURI(Uri.fromFile(file));

                    mFileUri = Uri.fromFile(file);

                    requestType = RC_IMAGE_CAPTURE;
                }
                break;
        }
    }

    private void uploadFromUri(Uri fileUri, int requestType) {
        Log.d(TAG, "uploadFromUri:src:" + fileUri.toString());

        // Save the File URI
        mFileUri = fileUri;

        // Clear the last download, if any
        mDownloadUrl = null;

        // Start MyUploadService to upload the file, so that the file is uploaded
        // even if this Activity is killed or put in the background
        startService(new Intent(this, MyUploadService.class)
                .putExtra(MyUploadService.EXTRA_FILE_URI, fileUri)
                .putExtra("REQUEST", requestType)
                .setAction(MyUploadService.ACTION_UPLOAD));

        // Show loading spinner
        showProgressBar(getString(R.string.progress_uploading));
    }


    private void insertarVistaFirestore() {
        if (validImage()){
            if (optionLatLng == null){
                if (spinnerLocation.getSelectedItemPosition() == 0){
                    optionLatLng = bundle.getParcelable(KEY_GEOLOCATION);
                }else {
                    optionLatLng = bundle.getParcelable(KEY_CHOSEN_LOCATION);
                }
            }
            uploadFromUri(mFileUri, requestType);
        }
    }

    private boolean validImage() {
        boolean valid = true;
        if(previewPicture == null || mFileUri == null){
            Toast.makeText(this, getString(R.string.imageErr), Toast.LENGTH_LONG).show();
            valid = false;
        }
        return valid;
    }

    private Map<String,Object> getViewDocument(Vista vista){
        Map<String,Object> document = new HashMap<>();
        document.put("animal", vista.getAnimal());
        document.put("location", vista.getLocation());
        document.put("photo", vista.getPhoto());
        document.put("userID", vista.getUserID());
        document.put("date", vista.getDate());
        document.put("description", vista.getDescription());

        return document;
    }




    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "cat.itb.krugerparkapp.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, RC_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.hacerFoto:
                selectImage(this);
                break;
            case R.id.btn_InsertarVista:
                insertarVistaFirestore();
                break;
        }
    }

    private void showMessageDialog(String title, String message) {
        AlertDialog ad = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .create();
        ad.show();
    }

    private void hideProgressBar() {
        mCaption.setText("");
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    private void showProgressBar(String caption) {
        mCaption.setText(caption);
        mProgressBar.setVisibility(View.VISIBLE);
    }
}