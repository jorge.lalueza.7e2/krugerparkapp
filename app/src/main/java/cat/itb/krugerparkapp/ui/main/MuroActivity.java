package cat.itb.krugerparkapp.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import cat.itb.krugerparkapp.R;
import cat.itb.krugerparkapp.adapter.VistaAdapater;

public class MuroActivity extends AppCompatActivity implements VistaAdapater.OnVistaSelectedListener{

    private MainViewModel mViewModel;
    private VistaAdapater mAdapter;
    private Query mQuery;
    private FirebaseFirestore firestore;

    private RecyclerView vistaRecycler;
    TextView nomMuro;
    TextView numMuro;
    String nomAnimal;
    int numAnimal;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.muro_activity);


        vistaRecycler = findViewById(R.id.recyclerFotosMuro);
        nomMuro = findViewById(R.id.titleMuro);


        mViewModel = new ViewModelProvider( this ).get( MainViewModel.class );

        firestore = FirebaseFirestore.getInstance();

        nomAnimal = getIntent().getStringExtra("animalName");

        nomMuro.setText(nomAnimal);

        String formattedAnimal = "#" + nomAnimal.toLowerCase();

        //Obtener lista de publicaciones
        mQuery = firestore.collection("vistas")
                .whereEqualTo("description", formattedAnimal)
                .orderBy("date", Query.Direction.DESCENDING);
        // RecyclerView
        mAdapter = new VistaAdapater(mQuery, this) {
            @Override
            protected void onDataChanged() {
                // Show/hide content if the query returns empty.
                if (getItemCount() == 0) {
                    vistaRecycler.setVisibility(View.GONE);
                    Snackbar.make(findViewById(android.R.id.content),
                            R.string.noAnimalsFound, Snackbar.LENGTH_LONG).show();
                } else {
                    vistaRecycler.setVisibility(View.VISIBLE);
                }
            }

            @Override
            protected void onError(FirebaseFirestoreException e) {

            }
        };
        vistaRecycler.setLayoutManager(new LinearLayoutManager(this));
        vistaRecycler.setAdapter(mAdapter);
    }




    public void onStart() {
        super.onStart();

        // Apply filters
        //onFilter(mViewModel.getFilters());

        // Start listening for Firestore updates
        if (mAdapter != null) {
            mAdapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }
    @Override
    public void onVistaClickedListener(DocumentSnapshot vista) {

    }
}