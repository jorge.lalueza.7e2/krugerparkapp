package cat.itb.krugerparkapp.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cat.itb.krugerparkapp.R;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    RecyclerView recyclerView;
    private TextView welcomeText;
    private FirebaseUser user;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated( view, savedInstanceState );
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        welcomeText = getView().findViewById(R.id.welcomeUser);

        MaterialButton btnVerMapa = getView().findViewById(R.id.btn_verMapa);
        btnVerMapa.setOnClickListener(this::cambiaraMapa);

        MaterialButton btnListaAnimales = getView().findViewById(R.id.btn_verListaAnimales);
        btnListaAnimales.setOnClickListener(this::cambiaraListaAnim);

        MaterialButton btnPublicaciones = getView().findViewById( R.id.btn_verPublis );
        btnPublicaciones.setOnClickListener( this::cambiarPublis );

        MaterialButton btnSignOut = getView().findViewById( R.id.btn_SignOut );
        btnSignOut.setOnClickListener( this::signOutAccount );

        user = FirebaseAuth.getInstance().getCurrentUser();
    }

    private void cambiarPublis(View view) {
        Navigation.findNavController( view ).navigate( R.id.action_mainFragment_to_timelineActivity );
    }

    private void signOutAccount(View view) {
        FirebaseAuth.getInstance().signOut();
        Navigation.findNavController( view ).navigate( R.id.action_mainFragment_to_logInActivity );
    }

    @Override
    public void onStart() {
        super.onStart();
        if (user != null) {
            Resources res = getResources();
            String welcomeString = String.format(res.getString(R.string.welcome), user.getDisplayName());
            welcomeText.setText(welcomeString);

        } else {
            Navigation.findNavController(getView()).navigate(R.id.action_mainFragment_to_logInActivity);
        }
    }

    private void cambiaraMapa(View view) {
        Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_mapsActivity);
    }

    private void cambiaraListaAnim(View view) {
        Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_listaFragment);
    }

}


