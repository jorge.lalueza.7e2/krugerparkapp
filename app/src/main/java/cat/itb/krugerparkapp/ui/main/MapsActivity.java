package cat.itb.krugerparkapp.ui.main;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cat.itb.krugerparkapp.R;
import cat.itb.krugerparkapp.model.KruegerUser;
import cat.itb.krugerparkapp.model.Vista;
import cat.itb.krugerparkapp.repository.FirestoreRepository;
import cat.itb.krugerparkapp.repository.SnapshotCallback;
import de.hdodenhof.circleimageview.CircleImageView;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerClickListener {

    private static final String KEY_VIEW_ID = "vista_id";
    private static LatLngBounds.Builder BUILDER = new LatLngBounds.Builder();
    LatLngBounds KRUEGER_BOUNDS;


//            new LatLng(),
//


    private Location mUserLocation;
    private GoogleMap mMap;
    private Marker geoLocMarker;
    private Marker userMarker;
    private LinearLayout container;

    //Card View
    private View myLayout;
    private ImageView postPicture;
    private TextView postTitle;
    private CircleImageView authorPicture;
    private TextView postAuthor;
    private ImageView closeCardButton;
    private ImageView arrow;
    ///


    private Map<DocumentSnapshot, Marker> usersMarkers;

    private DocumentSnapshot selectedVistaSnapshot;

    private FirestoreRepository firestoreRepository;

    private FirebaseAuth mAuth;

    private FirebaseUser firebaseUser;

    private FloatingActionButton btnAdd;
    private FloatingActionButton btnMyLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        BUILDER
                .include(new LatLng(-25.513846, 30.834682))
                .include(new LatLng(-22.129690, 32.640246));

        KRUEGER_BOUNDS = BUILDER.build();


        container = (LinearLayout) findViewById(R.id.container);

        btnAdd = findViewById(R.id.btn_Add);
        btnMyLocation = findViewById(R.id.myLocationBtn);

        btnAdd.setOnClickListener(this);
        btnMyLocation.setOnClickListener(this);
        btnAdd.hide();
        btnMyLocation.hide();

        MaterialButton btnSat = findViewById(R.id.typeMapSAT);
        MaterialButton btnHyb = findViewById(R.id.typeMapHYB);
        MaterialButton btnNor = findViewById(R.id.typeMapNOR);

        btnSat.setOnClickListener(this);
        btnHyb.setOnClickListener(this);
        btnNor.setOnClickListener(this);

        firestoreRepository = new FirestoreRepository(FirebaseFirestore.getInstance());
        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();
        usersMarkers = new HashMap<>();
        selectedVistaSnapshot = null;

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        if (status == ConnectionResult.SUCCESS) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, (Activity) getApplicationContext(), 10);
            dialog.show();
        }
    }

    private void cambiarTipoMapa(View view) {
        switch (view.getId()) {
            case R.id.typeMapNOR:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.typeMapHYB:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.typeMapSAT:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
        }
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        mMap = googleMap;
        mMap.setOnMapLongClickListener(this);
        mMap.setOnMarkerClickListener(this);
        miUbicacion();

        //Centrar vista en el parque
        mMap.addMarker(new MarkerOptions()
                .position(KRUEGER_BOUNDS.getCenter())
                .icon(BitmapDescriptorFactory.defaultMarker()));
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(KRUEGER_BOUNDS, 0));
        mMap.setLatLngBoundsForCameraTarget(KRUEGER_BOUNDS);

        //Cargar todas las ubicaciones de las publicaciones subidas por el usuario
        cargarUbicaciones();


    }

    private void DrawRectangle(LatLngBounds bounds) {

        LatLng northEast = bounds.northeast;
        LatLng southWest = bounds.southwest;

        final double delta = 0.01;

        LatLngBounds newBounds =
                new LatLngBounds.Builder()
                        .include(new LatLng(northEast.latitude - delta,
                                northEast.longitude - delta))
                        .include(new LatLng(southWest.latitude + delta,
                                southWest.longitude + delta))
                        .build();

        LatLng newBoundsNorthWest = new LatLng(newBounds.southwest.latitude, newBounds.northeast.longitude);
        LatLng newBoundsSouthEast = new LatLng(newBounds.northeast.latitude, newBounds.southwest.longitude);

        // add rectangle to a map
        PolygonOptions boundsArea = new PolygonOptions()
                .add(newBoundsNorthWest)
                .add(newBounds.northeast)
                .add(newBoundsSouthEast)
                .add(newBounds.southwest);
        mMap.addPolygon(boundsArea);
    }

    private void cargarUbicaciones() {
        firestoreRepository.getAllDocs("vistas", new SnapshotCallback() {
            @Override
            public void onSnapshot(DocumentSnapshot documentSnapshot) {
            }

            @Override
            public void onSnapshots(List<DocumentSnapshot> documentSnapshotList) {
                for (DocumentSnapshot snapshot : documentSnapshotList) {
                    Vista vista = snapshot.toObject(Vista.class);

                    GeoPoint geoPoint = vista.getLocation();
                    LatLng latLng = new LatLng(geoPoint.getLatitude(), geoPoint.getLongitude());

                    float color = getHueColor(vista.getAnimal().getPath());
                    Marker marker = mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(color))
                            .title(vista.getDescription()));

                    usersMarkers.put(snapshot, marker);
                }
            }
        });
    }

    public float getHueColor(String animalPath) {
        String[] arraystr = animalPath.split("/");
        String animal = arraystr[arraystr.length - 1];
        switch (animal) {
            case "antelope":
                return 0;
            case "buffalo":
                return 15;
            case "cheetah":
                return 30;
            case "crocodile":
                return 45;
            case "eland":
                return 60;
            case "elephant":
                return 75;
            case "giraffe":
                return 90;
            case "gnu":
                return 105;
            case "hippopotamus":
                return 120;
            case "hyena":
                return 135;
            case "impala":
                return 150;
            case "kudu":
                return 165;
            case "lion":
                return 180;
            case "rhino":
                return 195;
            case "warthog":
                return 210;
            case "wildDog":
                return 225;
            case "zebra":
                return 240;
        }
        return 360;
    }


    public void mostrarUbicacion(Location location) {
        if (location == null) {
            return;
        }
        LatLng coord = new LatLng(location.getLatitude(), location.getLongitude());
        if (!KRUEGER_BOUNDS.contains(coord)) {
            Toast.makeText(MapsActivity.this, R.string.outOfThePark, Toast.LENGTH_LONG).show();
            return;
        }
        CameraUpdate miUbi = CameraUpdateFactory.newLatLngZoom(coord, 10);
        if (geoLocMarker != null) geoLocMarker.remove();
        geoLocMarker = mMap.addMarker(new MarkerOptions()
                .position(coord)
                .title("Mi ubicación")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.gps_94631)));
        mMap.animateCamera(miUbi);
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                mUserLocation = location;
                mostrarUbicacion(location);
                updateAddBtnVisibility();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            updateAddBtnVisibility();

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
            mUserLocation = null;
            if (geoLocMarker != null && geoLocMarker.isVisible()){geoLocMarker.remove();}
            updateAddBtnVisibility();

        }
    };

    public void miUbicacion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            mUserLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            mostrarUbicacion(mUserLocation);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 0, locationListener);
        }

    }

    private void cambiarAdd() {
        Bundle args = new Bundle();
        if (mUserLocation != null) {
            args.putParcelable("GeoLoc", new LatLng(mUserLocation.getLatitude(), mUserLocation.getLongitude()));
        }
        if (userMarker != null) {
            args.putParcelable("CustomLoc", userMarker.getPosition());
        }
        Intent intent = new Intent(MapsActivity.this, AddActivity.class);
        intent.putExtras(args);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Add:
                cambiarAdd();
                break;
            case R.id.myLocationBtn:
                mostrarUbicacion(mUserLocation);
                break;
            case R.id.typeMapNOR:
            case R.id.typeMapHYB:
            case R.id.typeMapSAT:
                cambiarTipoMapa(v);
                break;
            case R.id.close_card:
                container.removeAllViews();
                selectedVistaSnapshot = null;
                break;
            case R.id.arrowSelectVista:
                Intent intent = new Intent(MapsActivity.this, TimelineActivity.class);
                intent.putExtra(KEY_VIEW_ID, selectedVistaSnapshot.getId());
                startActivity(intent);
                break;
        }
    }


    @Override
    public void onMapLongClick(LatLng latLng) {
        if (userMarker != null) {
            userMarker.remove();
            userMarker = null;
        }
        if (KRUEGER_BOUNDS.contains(latLng)) {
            Toast.makeText(MapsActivity.this,
                    "onMapLongClick:\n" + latLng.latitude + " : " + latLng.longitude,
                    Toast.LENGTH_SHORT).show();
            //Add marker on LongClick position
            userMarker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            createCustomMarker(MapsActivity.this, firebaseUser.getPhotoUrl())))
                    .title("My Marker"));
        } else {
            Toast.makeText(MapsActivity.this, "That's outside the national park !", Toast.LENGTH_SHORT).show();
        }
        updateAddBtnVisibility();
    }

    public static Bitmap createCustomMarker(Context context, Uri picture) {

        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);

        CircleImageView markerImage = (CircleImageView) marker.findViewById(R.id.user_dp);

        Glide.with(markerImage.getContext())
                .load(picture)
                .into(markerImage);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);

        return bitmap;
    }


    public void updateAddBtnVisibility() {
        if (mUserLocation != null || userMarker != null) {
            if (mUserLocation != null) {
                btnMyLocation.show();
            }
            btnAdd.show();
        } else {
            btnAdd.hide();
        }
        if (mUserLocation == null) {
            btnMyLocation.hide();
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        if (!marker.equals(userMarker) && usersMarkers.containsValue(marker)) {
            container.removeAllViews();

            selectedVistaSnapshot = null;
            for (Map.Entry<DocumentSnapshot, Marker> entry : usersMarkers.entrySet()) {
                Marker currentMarker = entry.getValue();
                if (currentMarker.equals(marker)) {
                    selectedVistaSnapshot = entry.getKey();
                }
            }

            myLayout = getLayoutInflater().inflate(R.layout.card_view, null);
            arrow = myLayout.findViewById(R.id.arrowSelectVista);
            postPicture = myLayout.findViewById(R.id.cardView_image);
            postTitle = myLayout.findViewById(R.id.postTiltle);
            authorPicture = myLayout.findViewById(R.id.authorPicture);
            postAuthor = myLayout.findViewById(R.id.postAuthor);
            closeCardButton = myLayout.findViewById(R.id.close_card);
            closeCardButton.setOnClickListener(this);
            arrow.setOnClickListener(this);
            Vista vista = new Vista();
            if (selectedVistaSnapshot != null) {
                vista = selectedVistaSnapshot.toObject(Vista.class);
            }

            if (vista != null) {

                Glide.with(postPicture.getContext())
                        .load(vista.getPhoto())
                        .into(postPicture);

                postTitle.setText(vista.getDescription());

                firestoreRepository.getDocumentByField("users", "id", vista.getUserID(), new SnapshotCallback() {
                    @Override
                    public void onSnapshot(DocumentSnapshot documentSnapshot) {
                        KruegerUser kruegerUser = documentSnapshot.toObject(KruegerUser.class);

                        Glide.with(getApplicationContext())
                                .load(kruegerUser.getPhotoUrl())
                                .into(authorPicture);
                        postAuthor.setText(kruegerUser.getDisplayName());
                    }

                    @Override
                    public void onSnapshots(List<DocumentSnapshot> documentSnapshotList) {

                    }
                });

                container.addView(myLayout); // you can pass extra layout params here too
                return true;
            }
            return false;
        }
        return false;
    }
}

