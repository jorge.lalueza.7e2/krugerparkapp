package cat.itb.krugerparkapp.ui.main;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.krugerparkapp.R;
import cat.itb.krugerparkapp.adapter.RecyclerViewAdapter;
import cat.itb.krugerparkapp.model.Animal;


public class ListaFragment extends Fragment {
    private RecyclerView recyclerView;
    public RecyclerViewAdapter listAdapter;
    MediaPlayer mediaPlayer = new MediaPlayer();

    public static ListaFragment newInstance() {
        return new ListaFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vista = inflater.inflate( R.layout.lista_fragment,container,false);
        recyclerView = vista.findViewById( R.id.animalsRecylerView);
        recyclerView.setLayoutManager( new LinearLayoutManager( getContext() ) );

        List<Animal> listcargada = obtenerAnimales();
        RecyclerViewAdapter adapter = new RecyclerViewAdapter( listcargada );
        recyclerView.setAdapter(adapter);

        adapter.setOnAnimalClicked(this::cambiarPantallaMuro);
        return vista;
    }

    private void cambiarPantallaMuro(Animal animal) {
        int numAnimal = animal.getNumberId();
        ejecutarSonido(numAnimal);
       String nomAnimal = animal.getName();

       Intent intent = new Intent(getActivity(), MuroActivity.class);
       intent.putExtra("animalName", nomAnimal);
       startActivity(intent);
    }

    private void ejecutarSonido(int numAnimal) {
        switch (numAnimal) {
            case 1:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.impalasound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;
            case 2:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.cebrasound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 3:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.bufalosound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 4:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.kudusound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
            case 5:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.elefantesound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
            case 6:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.nusound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 7:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.soundjirafa);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 8:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.hienasound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 9:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.impalasound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 10:
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.cocodrilosound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 11:
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.facoquerosound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 12:
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.hipopotamosound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 13:
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.soundleon);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 14:
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.rinocerontesound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 15:
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.soundleopardo);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 16:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.rinocerontesound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 17:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.elandsound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 18:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.licaonsound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

            case 19:
                mediaPlayer.stop();

                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.guepardosound);
                if (!mediaPlayer.isPlaying()) {
                    mediaPlayer.start();
                }
                break;

        }
    }

    private List<Animal> obtenerAnimales (){
        List<Animal> animalList = new ArrayList<>();

       animalList.add( new Animal( 1,"Impala",R.drawable.impala,100000, Color.BLUE ));
       animalList.add( new Animal( 2,"Zebra",R.drawable.cebra,30000, Color.BLUE ));
        animalList.add( new Animal( 3,"Buffalo",R.drawable.bufalo,15000, Color.BLUE ));
        animalList.add( new Animal( 4,"Kudu",R.drawable.kudu,13800, Color.BLUE ));
        animalList.add( new Animal( 5,"Elephant",R.drawable.elefante,13700, Color.BLUE ));
        animalList.add( new Animal( 6,"Gnu",R.drawable.nu,13000, Color.BLUE ));
        animalList.add( new Animal( 7,"Girraffe",R.drawable.jirafa,8300, Color.BLUE ));
        animalList.add( new Animal( 8,"Hyena",R.drawable.hiena,5000, Color.BLUE ));
        animalList.add( new Animal( 9,"Antelope",R.drawable.antilope,4900, Color.BLUE ));
        animalList.add( new Animal( 10,"Crocodile",R.drawable.cocodrilo,4400, Color.BLUE ));
        animalList.add( new Animal( 11,"Warthog",R.drawable.facoquero,4000, Color.BLUE ));
       animalList.add( new Animal( 12,"Hippopotamus",R.drawable.hipopotamo,3000, Color.BLUE ));
        animalList.add( new Animal( 13,"Lion",R.drawable.leon,2000, Color.BLUE ));
        animalList.add( new Animal( 14,"Rhino",R.drawable.rinoblanco,2000, Color.BLUE ));
       animalList.add( new Animal( 15,"Lepard",R.drawable.leopardo,1000, Color.BLUE ));
        animalList.add( new Animal( 16,"Eland",R.drawable.eland,460, Color.BLUE ));
        animalList.add( new Animal( 17,"WildDog",R.drawable.licaon,400, Color.BLUE ));
        animalList.add( new Animal( 18,"Cheetah",R.drawable.guepardo,300, Color.BLUE ));
        return animalList;
    }


}
