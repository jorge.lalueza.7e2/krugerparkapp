﻿# TRELLO: https://trello.com/b/cqfbHPGw/krugerpark-app

## IMPORTANT: Per poder iniciar sessió amb el compte de Google, és necessari, canviar les **variants del build** de 'debug' a **'release'**, o instalar el fitxer ***apk***, que es troba a ***app/release***, hem hagut de crear una keystore y una key, perquè les aplicacions que usen el signIn de Google han de estar certificades. En cas contrari no ens permetrà inicar sessió ja que hauria falta la SHA-1 key de l'Android Studio del professor.

## COM EXECUTAR L'APP A L'ANDROID STUDIO (RELEASE):
A la cantonada inferior esquerra trobem la pestanya de ***Build Variants***, clickem i a la columna de Active Build Variant seleccionem relase en comptes de debug, el projecte ja té la carpteta keysotre amb la seva key.

![alt text](img/build_variants.JPG "Build Variants")


# INICI I LOGIN

La nostra app "KrugerPark app", té una pantalla principal que només es mostrarà si l'usuari no hagi iniciat sessió inicialment.
Aquesta pantalla consisteix en un botó de Google, que al fer click podras seleccionar amb quin compte vols iniciar.

Una vegada ja iniciada la sessió, navegarà a la pantalla principal, la app carregarà el seu nom i la seva foto.
En aquesta app hi han 3 botons implementats.

# VER MAPA
	Aquest botó et fa navegar al Maps Activity.
	- Dintre d'aquest a la part superior ens trobem 3 botons, que la seva funcionalitat és cambiar el tipus de mapa.
	- La app demanarà el permís per agafar l'ubicació de l'usuari, una vegada acceptat el mapa es centrarà en aquest i fara zoom. (Probat amb Fake GPS)
	- També hem fet que aquesta ubicació si et mous s'actualitzi, i hem marcat "KrugerPark" com a un marcador.
	- A la part inferior trobem el boto "+", que fara navegar al Add Activity.

# ADD ACTIVITY
		En aquesta activity seleccionarem la informació que volem insertar.
		- Primer trobem un desplegable amb la selecció de tots els animals que hi han al parc.
		- Un textView que agafarà la ubicació de l'usuari automàticament.
		- Un ImageView que és on carregarà la foto posteriorment.
		- Un botó " HACER FOTO ", et deixarà escollir entre accedir a la Galeria o a la Camara.
		- Una vegada seleccionada la foto o feta, es posicionarà al ImageView.
		- Finalment tenim el botó " INSERTAR VISTA ", que s'encarrega d'inserir aquesta vista al Maps activity.
		Depenent l'animal el marcador serà de colors diferents.


# VER LISTA ANIMALES
	Aquest botó et fa navegar a la llista d'animals.
	- Hi ha un recyclerView amb tots els animals.
	- Si fas click a qualsevol, s'executa el seu so i entra al mur d'aquest animal.
	- Al mur de cada animal sortiran només les seves publicacions.

# VER PUBLICACIONES
	Aquest botó et fa navegar al timeline de Kruger.
	- Es mostraràn les publicacions per ordre de pujada.
	- Es mostrarà l'usuari que les penja.
	- A cada publicació podem interacturar amb el botó d'opcions.

# OPCIONS
		Al fer click en aquest botó s'obrira un panel.
		Amb les opcions següents:
			- ELIMINAR: Només podrà eliminar l'app l'usuari que ha penjat aquesta.
			- COMPARTIR: S'obrirà un panel per seleccionar a quina xarxa vols compartir-la.
			- DENUNCIAR: Denunciar la publicació.
			- CANCELAR: Tancar l'alerta.
